# RSandBox ![KPI logo](https://gitlab.com/4neko/rsandbox/-/raw/master/logo_600.png?ref_type=heads&inline=true)

## This software is still under development. This is read only mirror to share the results with other developers who does not have access to private repo or for feedback only!

A new sandbox software which is specially designed for FreeBSD and which is designed to increase the isolation of software from other assets in the system.  

The main purpose is to reduce the ability of some programs which may run under root to make any changes in the system.

The previous project [Sandbox](https://gitlab.com/relkom/sandbox) is a legacy and will be replaced with this one.

Based on: 
- Rust (both userland and kernel module)
- Mac Framework (with additional, optional patches)


It is planned to support only the latest FreeBSD version.  

## Issues

Based on a expirience developing previous project [Sandbox](https://gitlab.com/relkom/sandbox) using MAC Framework, it was found out that there are some limitations which can not be fixed without additional patches.

Witout patches the procedures like `file-*` or other which anyhow involves the convertion of the `VNode` to fs-path, will not accept filters. It will not be possible to limit percisly.

...


## Scheme example

```lisp
(version 2) ; version of scheme parser

(scheme "root"
    (deny default)

    (auto network-bind
        (match-any "group 1 text description"
            (net-addr tcp "192.168.0.0/24:*-100")
            (net-addr unix "/var/run/prog/cmd.sock")
        )
    )

    (allow network-inbound
        (net-addr-local any "192.168.0.174:0")
        (net-addr-remote tcp "192.168.1.175:8080")
        (net-addr-remote tcp "192.168.1.155:*-500")
        (net-addr-local unix "/var/run/sock.sock")
    )
)

(scheme 2u..10u
    (deny default)

    (auto system-kldload
        (match-any
            (regex "/usr/local/drive/[abc]")
            (literal "/boot/modules/driver1.ko")
        )
    )
)
```
